<?php

require_once __DIR__ . '/../vendor/autoload.php';
use App\App;
use App\Router;
use App\Controllers\HomeController;
use Illuminate\Container\Container;

$container = new Container();
$router = new Router($container);

$router->registerRouteFromControllerAttributes([
    HomeController::class
]);

(new App(
    $container,
    $router,
    ['uri' => $_SERVER['REQUEST_URI'], 'method' => $_SERVER['REQUEST_METHOD']],
))->run();