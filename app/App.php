<?php

namespace App;

use App\Contracts\EmailContract;
use App\Contracts\InvoiceContract;
use App\Contracts\PaymentGatewayContract;
use App\Contracts\SalesTaxContract;
use App\Exceptions\RouteNotFoundException;
use App\Services\EmailService;
use App\Services\InvoiceService;
use App\Services\PaymentGatewayService;
use App\Services\SalesTaxService;
use Illuminate\Contracts\Container\Container;

class App
{
    public function __construct(
        protected Container $container,
        protected Router $router,
        protected array $request
    )
    {
        $this->container->bind(SalesTaxContract::class, SalesTaxService::class);
        $this->container->bind(PaymentGatewayContract::class, PaymentGatewayService::class);
        $this->container->bind(EmailContract::class, EmailService::class);
        $this->container->bind(InvoiceContract::class, InvoiceService::class);
    }

    public function run()
    {
        try {
            echo $this->router->resolve($this->request['uri'], strtolower($this->request['method']));
        } catch (RouteNotFoundException) {
            http_response_code(404);

            echo "404 | Route not found";
        }
    }
}