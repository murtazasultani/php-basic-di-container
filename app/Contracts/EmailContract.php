<?php

namespace App\Contracts;

interface EmailContract
{
    public function send(array $to, string $subject, string $message): bool;
}