<?php

namespace App\Contracts;

interface PaymentGatewayContract
{
    public function charge(array $customer, float $amount, float $tax): bool;
}