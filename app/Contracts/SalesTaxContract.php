<?php

namespace App\Contracts;

interface SalesTaxContract
{
    public function calculate(float $amount, array $customer): float;
}