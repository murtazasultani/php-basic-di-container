<?php

namespace App\Contracts;

interface InvoiceContract
{
    public function __construct(
        SalesTaxContract $salesTaxService,
        PaymentGatewayContract $gatewayService,
        EmailContract $emailService
    );

    public function process(array $customer, float $amount): bool;
}