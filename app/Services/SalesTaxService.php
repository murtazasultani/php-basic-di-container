<?php

namespace App\Services;

use App\Contracts\SalesTaxContract;

class SalesTaxService implements SalesTaxContract
{
    public function calculate(float $amount, array $customer): float
    {
//        TODO: Will be updated later
        return $amount * 0.2 / 100;
    }
}