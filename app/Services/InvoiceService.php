<?php

namespace App\Services;

use App\Contracts\EmailContract;
use App\Contracts\InvoiceContract;
use App\Contracts\PaymentGatewayContract;
use App\Contracts\SalesTaxContract;

class InvoiceService implements InvoiceContract
{
    public function __construct(
        protected SalesTaxContract $salesTaxService,
        protected PaymentGatewayContract  $gatewayService,
        protected EmailContract $emailService,
    )
    {
    }

    public function process(array $customer, float $amount): bool
    {
        $tax = $this->salesTaxService->calculate($amount, $customer);

        if (! $this->gatewayService->charge($customer, $amount, $tax)) {
            return false;
        }

        $this->emailService->send($customer, 'Receipt', 'Invoice processed');

        echo 'Invoice has been processed<br />';

        return true;
    }
}