<?php

namespace App\Controllers;

use App\Attributes\Route;
use App\Contracts\InvoiceContract;

class HomeController
{
    public function __construct(
        private InvoiceContract $invoiceService,
    )
    {
    }

    #[Route('/')]
    public function index(): string
    {
        return $this->invoiceService->process([], 12);
    }

    #[Route('/home')]
    public function home()
    {
        echo "Hello from PHP Attributes!";
    }
}